# Troubleshooting

## Chromium keeps asking me to restore tabs

This happens because Chromium is unceremoniously killed when Majel is killed.
You can safely suppress this request by doing the following:

1. Go to the `Closed Tab Cache` option in Chromium flags: `chrome://flags/#closed-tab-cache`
2. Mark it as Disabled
3. Close Chromium


## I can't install from PyPI on a Raspberry Pi

When you try to install a Python project across architectures, things get...
tricky.  Many projects, it would seem, will often not release packages for
`aarch64` or `armv7l` on PyPI, instead pushing them to GitHub or [piwheels.org](https://piwheels.org/).
This means that if you're going to install this on a non-x86 machine, there's
likely to be a couple bumps you might have to smooth out.


### `armv7l`

The default Raspberry Pi OS runs the 32-bit `armv7l` architecture (even though
modern Pis are 64-bit capable).  The only package that's proven itself to be a
problem as been `opencv-python`, which doesn't have `armv7l` wheels on [PyPI](https://pypi.org/project/opencv-python/#files),
but *does* have some on [piwheels.org](https://www.piwheels.org/project/opencv-python/).

Thankfully, because of how Raspberry Pis are typically set up to include
piwheels as an extra source for wheels, installation should be as easy as:

```shell
$ pip install majel
```

If however you're wanting to *develop* on your Pi, this project's use of Poetry
bumps up against [an annoying bug](https://stackoverflow.com/questions/70564200/how-to-use-python-poetry-across-architectures)
in that project, so development will unfortunately have to look something like
this:

```shell
$ git clone <your fork of this project> majel
$ cd majel
$ poetry build
$ pip install dist/majel-<version>-py3-none-any.whl
[Make your changes]
$ poetry build
$ pip install dist/majel-<version>-py3-none-any.whl
```

...repeat until you have what you need.  I know, it sucks, so if you've got a
better idea, please share it.


## `aarch64`

If you're using a Raspberry Pi with a 64-bit OS like Manjaro, then
`opencv-python` *is* available on PyPI but `vosk-api` [isn't](https://pypi.org/project/vosk/#files).
For that package, you need to source the `aarm64` wheel from their git
repo.  The reasoning for this hasn't been made clear to me yet, but I'm having
a conversation with them about this [here](https://github.com/alphacep/vosk-api/issues/479#issuecomment-1002715227).
Until this is fixed though, installing Majel on an aarch64-based system should
either be done with Arch/Manjaro's `yay -S majel` (which accounts for this
oddity in the installer) or in three steps the old-fashioned way:

```shell
# Install stuff the Python dependencies need on the system.
$ yay -S gobject-introspection gnome-shell scrot tk

# Install Vosk directly from GitHub
$ pip install https://github.com/alphacep/vosk-api/releases/download/v0.3.32/vosk-0.3.32-py3-none-linux_aarch64.whl

# Install majel
$ pip install majel
```

Similarly, if you're developing on Majel using Poetry, you'll need a few extra
steps as well:

```shell
# Install all of the dependencies but fail on vosk
$ poetry install

# Hop into your new virtualenv
$ poetry shell

# Install vosk from GitHub
$ pip install https://github.com/alphacep/vosk-api/releases/download/v0.3.32/vosk-0.3.32-py3-none-linux_aarch64.whl

# Install majel now that all dependencies are in place
$ poetry install
```

!!! note

    The version numbers used in the above URLs may change over time.  You'll
    need to note what version Majel is trying to install and amend the URL to
    suit before installing.
