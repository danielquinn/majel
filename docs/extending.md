# Extending Majel

Extending Majel is pretty easy.  Everything is object-oriented and broken up
into what is (hopefully seen as) logical subsystems.  Most people will just
want to write their own handlers, and that may actually not require any code
at all!

!!! note

    This section depends heavily on the [architecture](architecture.md)
    section, so you should make sure you've read that first.


## Writing Custom Handlers

Majel has something called the `UserHandler` which is special handler that
looks for folders in `${HOME}/.config/majel/commands`, then looks inside those
folders for a file called `command.yaml`.  This file is the only thing you need
to have your own custom handler.

For example, consider the following stored in a file called
`${HOME}/.config/majel/commands/awesome/command.yaml`:

```yaml
command: do a hello world
steps:
  - type: command
    argument: open gedit
  - type: wait
    argument: 2
  - type: type
    argument: Hello world!
```

Just having this file there with these contents would mean that Majel could
accept the command `do a hello world`, and it would open gedit and type
`Hello world!`.

The API is reasonably broad here, supporting four different actions:

| Action  | Type    | How it works                                                                                                                                                                                                                                      |
|---------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| command | string  | Sends the defined string to Majel's command handler as if you'd just spoken it.  Be sure not to use any punctuation or capitalisation here as it's likely the handlers won't understand what to do with that sort of thing.                       |
| click   | path    | This takes the defined string as an image file path relative to the folder it's in.  This image file is then passed to pyautogui to say "look for this file and click on it".  Note that for best results, this should probably be a `.png` file. |
| type    | string  | Types exactly what you've defined as the `argument` out as if you were typing it in real time.                                                                                                                                                    |
| wait    | integer | Do nothing for exactly this many seconds.                                                                                                                                                                                                         |

For a lot of things, this should be enough, but if you need to get more
advanced, or if you just think that your idea is awesome enough to want to
share, you'll need to look at extending a handler in Python.


## Advanced Custom Handlers

Majel loads handlers by way of looking at the parent `Handler` class and
finding all the child classes in memory *when the manager is started*.  That
means that if you want your handler to be available you must do two things:

1. Extend the `Handler` base class, implementing (at the very least)
   `.can_handle()` and `.handle()`.
2. Make sure your handler is in memory before the manager starts (which is
   pretty early).  The easiest way to do this is to simply import it in the
   `__init__.py` file in the `handlers` folder.

Once you've satisfied the two above conditions, Majel should report the loading
of your handler at start time and you should be able to trigger it in cases
where your `.can_handle()` method returns `True`.


### An Example

Here's a simple example of a handler that lets you say `Google [something]`.
It will open Firefox and take you to Google's search page.

```python
from time import sleep

import pyautogui

from majel.handlers.base import Handler


class GoogleHandler(Handler):

    def __str__(self):
        return "Google"

    def can_handle(self, command: str) -> bool:
        return command.startswith("google ")

    def handle(self, command: str) -> None:

        # Take the command, without "google "
        query = command[7:]

        # Use the OpenHandler to open Firefox.  `self.manager.execute()` can be
        # used to invoke any other handler by writing out the command that
        # handler is expected to handle.
        self.manager.execute("open firefox")

        # Wait a short time for slower machines
        sleep(0.5)

        # Use the `Ctrl+l` keyboard shortcut to access the location bar
        pyautogui.hotkey("ctrlleft", "l")

        # Type the URL we want and hit enter
        pyautogui.typewrite(list(f"https://google.com/?q={query}") + ["enter"])
```

From here you should be able to come up with all sorts of crafty things to do.
If you're looking for inspiration or just hoping not to reinvent the wheel have
a look at the built-in handlers where you can see patterns for switching
workspaces (just extend `WorkspaceHandler` instead of just `Handler`), using
the TTS system (look at the `SayHandler` for that one), or how to leverage some
of the available helper modules (the `KodiHandler` is a good one for this).


## Writing Your Own Plugins

The problem with the above is that it's not easy to satisfy #2 on that list of
requirements without making a pull request into Majel and having your code
merged.  Maybe you'd like to build your project as an external dependency to be
loaded or not based on user interest.  If that's you, keep reading.

Majel uses a crude system for plugins.  Basically if your module name starts
with `majel`, we assume that this is a Majel plugin and then we load your
module's `__init__.py` file at start time, ensuring that it's around for the
subclass detection we mentioned above.

In practise, this means that your project might look something like this given
the example above as a starting point:

```
.
├─ majel_google
│  ├─ __init__.py
│  └─ handlers.py
└─ setup.py
```

Your `handlers.py` would be as above, and your `__init__.py` would look like
this:

```python
from .handlers import GoogleHandler
```

So long as your `GoogleHandler` is imported in `__init__.py`, Majel should see
your handler at start time and announce that it's been loaded in its start-up
logs.
