# Installation

Majel is just a Python wrapper around stuff you already have installed on your
system.  So, if you want Majel to control something, you first have to install
that something along with a few tools Majel uses to do the controlling.


## Quickstart: Just use Arch Linux

Arch Linux is supported because packaging software for the AUR is so very easy.
One day, I hope to figure out how to package this for Flatpak, but so far I've
not been very successful.

```shell
$ yay -S majel
```

This will install everything you need, including Mimic, Firefox and Kodi.  Now
you just have to [configure](configuration) it.


## Doing things the hard way

Maybe you're not running Arch, or maybe you want to make this a Flatpak (yay!).
Either way, there's a few things that Majel needs to function:

1. **tk**: This is required by [PyAutoGUI](https://pyautogui.readthedocs.io/en/latest/index.html),
   for mouse automation which Majel uses heavily.
2. **gobject-introspection**: A facility that allows Python to talk to the
   inner workings of GNOME and do things like get a list of keyboard shortcuts
   to use for switching screens etc.
3. **GNOME**: Everything Majel does is based on the expectation that you're
   running [GNOME](https://gnome.org/).  Maybe one day it can be modified to
   work on other desktop environments, but for the moment, GNOME is supported
   'cause that's what I use.
4. **scrot**: Is a lightweight screen-shotting program that [PyAutoGUI](https://pyautogui.readthedocs.io/en/latest/index.html)
   uses to find what we want to click.
5. Development libraries for things like `cairo` and `portaudio`.  In Ubuntu,
   you'll want `libcairo2-dev`, `libgirepository1.0-dev`, and
   `portaudio19-dev`.  You'll also need `python3-dev`.


### Optional Dependencies

1. **Firefox**: Not strictly necessary, but much of what Majel can do is done
   via a browser.
2. **Chromium**: Like Firefox, it's not strictly necessary, but if you want to 
   control DRM-encumbered streaming services like Netflix and Amazon Prime, you
   need this.  If you're installing on an aarch64 machine (like a 64-bit
   Raspberry Pi) you should install `chromium-docker`, since the standard
   Chromium won't support Widevine for DRM-encumbered video streams.
3. **Kodi**: If you want Majel to control your local media library, the way
   it's expecting to do that is with Kodi, so that needs to be there in this
   case.
4. **Mimic**: This is a project that came out of Mycroft that does some
   excellent voice synthesis.  Without this, Majel will not be able to speak.
   Truth be told, unless you're asking it questions for which you expect a
   voiced answer, this may not be a problem for you.


### Building Majel

It's a Python project, so all you really need to do is install it from PyPI:

```shell
$ pip install majel
```

This will install Majel along with all of its Python dependencies, after which
point you can start it with `majel`, but before you do that, you'll want to
[configure](configuration) it.