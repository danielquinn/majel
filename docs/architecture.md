# Architecture

Majel is just a dumb loop waiting for a trigger to tell it to activate the
listener.  When that happens, the listener collects audio until it doesn't hear
speech anymore, and then passes what it heard to a series of "handlers" to
determine if what it heard was something that can be um... *handled*.

If this isn't enough detail for you, keep reading.


## The Command Loop

When you run `majel`, you're running a small command-line program that invokes
a few daemonised threads: the [manager](#the-manager), the [listener](#the-listener),
and the [recorder](#the-recorder).  It will also attempt to load any [plugins](extending.md#writing-your-own-plugins)
you may have also installed.  After that, it just sits in an infinite loop,
waiting for you to hit `Ctrl+C`.


## The Manager

The manager is a daemonised thread that finds all of the handlers currently
installed, checks if they're capable of starting (typically this depends on)
whether you've [configured](configuration.md) them properly and if the required
software (like Kodi for example) is installed), and if they are, it starts each
of them.  It also keeps track of each [handler](#handlers) thread, so it can do
things like issue the `stop` command to all handlers at once should you ask for
that.

This means that if you're listening to music with Kodi while reading a recipe
for lemon butter chicken in Firefox, you can say "stop" and it will kill the
music.  The manager also issues the shutdown order to all handlers when you
kill the parent [command](#the-command-loop).


## The Listener

The listener doesn't listen for audio, but rather "listens" to a local Unix
socket for whatever comes down the pipe.  It's this socket that the
`majel-controller` command writes to.

This means that if you run `majel-controller listen`, the listener will start
the audio [recorder](#the-recorder).  Similarly `majel-controller stop` will
send the `stop()` event to all handlers, and anything something like
`majel-controller 'open nautilus'` will just pass `open nautilus` to the [manager](#the-manager)
to be handled by the `OpenHandler`.


## The Recorder

The recorder is a fiddly bit of software the leverages `PyAudio` and
`webrtcvad` record the sound in the room and parse it for speech respectively.
This is where we figure out when you've started talking and when you've
stopped, collect that noise into an audio file and then pass that file to the
callback method (defined in [the command loop](#the-command-loop), which is
simply passes this file our speech-to-text (STT) system (Vosk), takes the text
output and passes that to the manager as the command to be handled.


## Handlers

Finally, the handlers are the most complicated part of Majel because they can
essentially do anything you want them to.  They have a standard interface of
just four methods:

### `.can_handle()`

This method takes the command offered as a string and returns `True` if this
handler is *capable* of handling the command.  Capability is determined by a
few factors:

* Is the handler operable/enabled?  For example, `NetflixHandler.can_handle()`
  won't return `True` for `watch the good place on netflix` if it hasn't been
  enabled in your [configuration](configuration.md).  This can be handy if
  you've got more than one handler capable of handling the same command.
* Is the handler even capable of running?  To use our Netflix example, even if
  you've configured it to run, this method will *still* return `False` if you
  don't have `chromium`, `chromium-docker`, or `google-chrome` installed.


### `.handle()`

The interesting stuff.  This is where you take the `command` argument and
perform an action with it.  Typically this would involve switching to the
expected workspace and performing a few mouse or keyboard actions with
`pyautogui`, but it can be *anything you want*.  You could have a
`ToastHandler` for example that accepts the `make toast` command and sends an
API request to your toaster to do its thing.


### `.stop()`

This does what it says on the tin and is entirely optional to implement, though
if your handler plays any sort of audio, it's a good idea to support this.

If a user issues the `stop` command, this method will be called.  You can use
it to click the pause button on your media player for example.


### `.shutdown()`

Typically you won't need to implement this, but if your handler needs to do
something *as it's shutting down* — such as cleaning up temporary files — then
this is where you do that.
