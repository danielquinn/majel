# Configuration

Majel is configured by way of a single `.yaml` file that is auto-generated on
its first run.  It will look something like this:

```yaml
version: 1

voice:

  vosk:
    source: vosk-model-small-en-us-0.15

  preferences:
    tts: mimic
    stt: vosk

handlers:

  browser:
    is_enabled: y

  prime:
    region: gb
    is_enabled: y
    workspace: 6

  netflix:
    is_enabled: y
    workspace: 7

  youtube:
    key: SEE_INSTRUCTIONS_BELOW
    is_enabled: y
    workspace: 8

  kodi:
    username: kodi
    password: kodi
    endpoint: http://localhost:8080/jsonrpc
    is_enabled: y
    workspace: 9

apis:

  utelly:
    key: SEE_INSTRUCTIONS_BELOW
    service_order:
      - netflix
      - prime
    country: uk
    is_enabled: y
```

Feel free to copy & paste the above into `${HOME}/.config/majel/config.yaml` if
you so choose.

Each section is explained below.


## `version`

This number increments whenever we need to change the structure of the file.
Hopefully that won't happen often if ever, but this is a good precaution to
take.


## `voice`

One day, Majel may grow to support all kinds of voice interfaces provided by
different libraries and companies.  For the moment though, the options are few.


### `vosk`

Majel's default subsystem for translating speech to text (STT) is [Vosk](https://alphacephei.com/vosk/),
an efficient STT library with a fantastically developer-friendly API.

| Parameter | Type     |     | Default                 | Options                                                       | Notes                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|-----------|----------|-----|-------------------------|---------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| source    | string   | No  | `vosk-model-en-us-0.22` | See [Vosk's models list](https://alphacephei.com/vosk/models) | By default, we assume you're speaking North American English on a reasonably powerful computer (any laptop built in the last 5 years), so we've selected the `vosk-model-en-us-0.22` model.  If you're running Majel on a Raspberry Pi, you may want to use `vosk-model-small-en-us-0.15` instead, or if you're speaking French you may want `vosk-model-fr-0.22`.  [Have a look at their site](https://alphacephei.com/vosk/models) for the list of available models.  Just copy & paste the name into this box and restart Majel.  It should download the model in question, unpack it, and store it in `${HOME}/.cache/majel/voices/vosk/` for use when trying to recognise your voice. |


### `preferences`

Currently, Majel only supports Vosk & Mimic for STT and TTS respectively.  Over
time, if Majel grows to support other subsystems like Mozilla, Google, and
Microsoft for these jobs, you'll be able to specify your preference for their
use here.

| Parameter | Type     | Required | Options | Default | Notes |
|-----------|----------|----------|---------|---------|-------|
| tts       | string   | No       | `mimic` | `mimic` |       |
| stt       | string   | No       | `vosk`  | `vosk`  |       |


## `handlers`

Some [handlers](architecture.md#handlers) allow for a few configuration
options.


### `browser`

This is a generic Firefox handler.  If enabled, you can use it to search your
bookmarks and do simple things like visit arbitrary URLs.

| Parameter  | Type    | Required | Options         | Default | Notes                                                                                                                                                                                                                                                                                                                                                                                                                    |
|------------|---------|----------|-----------------|---------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| profile    | string  | No       |                 | `Majel` | The browser profile you want to use.  Rather than using your normal profile,  the default for this is `Majel`, which means on first run Majel will create a separate browser window with a separate set of preferences, and no connection to your Firefox account.  If you don't like this, you can change this to `default` or open `about:profiles` in Firefox to get the name of the profile you'd like Majel to use. |
| workspace  | integer | No       |                 | `1`     | The workspace you want Firefox to appear on.                                                                                                                                                                                                                                                                                                                                                                             |
| is_enabled | boolean | Yes      | `true`, `false` | `false` | By default, this handler is *not* enabled, and so Firefox won't open to be controlled by Majel.  Set this to `true` to turn it on.                                                                                                                                                                                                                                                                                       |


### `netflix`

| Parameter  | Type    | Required | Options         | Default   | Notes                                                                                                                              |
|------------|---------|----------|-----------------|-----------|------------------------------------------------------------------------------------------------------------------------------------|
| profile    | string  | No       |                 | `Netflix` | This is the name of the Chromium profile you want to use for Netflix.                                                              |
| workspace  | integer | No       |                 | `2`       | The workspace you want Netflix to appear on.                                                                                       |
| is_enabled | boolean | Yes      | `true`, `false` | `false`   | By default, this handler is *not* enabled, and so Netflix won't open to be controlled by Majel.  Set this to `true` to turn it on. |


### `prime`

| Parameter  | Type    | Required | Options         | Default | Notes                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|------------|---------|----------|-----------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| profile    | string  | No       |                 | `Prime` | This is the name of the Chromium profile you want to use for Amazon Prime Video.                                                                                                                                                                                                                                                                                                                                                                                              |
| workspace  | integer | No       |                 | `3`     | The workspace you want Amazon Prime Video to appear on.                                                                                                                                                                                                                                                                                                                                                                                                                       |
| is_enabled | boolean | Yes      | `true`, `false` | `false` | By default, this handler is *not* enabled, and so Amazon Prime Video won't open to be controlled by Majel.  Set this to `true` to turn it on.                                                                                                                                                                                                                                                                                                                                 |
| region     | string  | No       | `gb`            | `gb`    | The region you're in.  We need this because Amazon has a different URL for every country it operates in, and we need to know which one to take you to since Prime accounts are region-locked.  Currently, only `gb` will work, which maps to https://www.amazon.co.uk/gp/video/storefront/, since as a UK resident, that's the only one I know about.  If you live somewhere else and have a different URL you'd like included, please send a merge request or open an issue. |


### `youtube`

| Parameter  | Type    | Required | Options         | Default   | Notes                                                                                                                                                                                                                         |
|------------|---------|----------|-----------------|-----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| profile    | string  | No       |                 | `YouTube` | This is the name of the Chromium profile you want to use for YouTube.                                                                                                                                                         |
| workspace  | integer | No       |                 | `4`       | The workspace you want Netflix to appear on.                                                                                                                                                                                  |
| is_enabled | boolean | Yes      | `true`, `false` | `false`   | By default, this handler is *not* enabled, and so YouTube won't open to be controlled by Majel.  Set this to `true` to turn it on.                                                                                            |
| key        | string  | Yes      |                 |           | Searching YouTube requires a free API key provided by Google.  Follow the instructions [here](https://developers.google.com/youtube/v3/getting-started) for how to get one.  Without a key, YouTube search will not function. |


### `kodi`

Kodi, if installed, is controlled by way of its official [JSON-RPC API](https://kodi.wiki/view/JSON-RPC_API/v12).
This API requires a `username` & `password` to authenticate and the `endpoint`
will almost always be `http://localhost:8080/jsonrpc` unless you're doing
something creative like trying to get Majel to control an instance of Kodi 
running on another machine.

You can enable this in Kodi by going to:

```
Settings → Services → Control → Allow remote control via HTTP
```

Even if you specify a `username`, `password`, and `endpoint`, Majel cannot
control Kodi unless you enable these controls via Kodi's configuration menus.

| Parameter  | Type    | Required | Options         | Default                         | Notes                                                                                                                           |
|------------|---------|----------|-----------------|---------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| workspace  | integer | No       |                 | `5`                             | The workspace you want Kodi to appear on.                                                                                       |
| is_enabled | boolean | Yes      | `true`, `false` | `false`                         | By default, this handler is *not* enabled, and so Kodi won't open to be controlled by Majel.  Set this to `true` to turn it on. |
| username   | string  | Yes      |                 |                                 | The username you gave Kodi.  The default is `kodi`.                                                                             |
| password   | string  | Yes      |                 |                                 | The password you gave Kodi.  Older versions of Kodi may allow for a blank password, but modern versions will insist on one.     |
| endpoint   | string  | Yes      |                 | `http://localhost:8080/jsonrpc` | Majel will start Kodi running locally, so this will almost always be best left as the default.                                  |


## `apis`

In addition to your handlers, some APIs that Majel accesses in the background
to do its job requires some credentials to function.


### Utelly

The Utelly API is a nifty free service that allows you to ask things like:

> *"Which streaming services offer 'The Expanse'?"*

It then responds with a list of those services and the URLs at which you can
find the show/movie in question.  Majel uses this API to handle requests like
`play the expanse`.  Without it, Majel won't be able to do anything with
Netflix or Amazon Prime Video.  Communication with Kodi will not be affected
however as it's local.

The API is free to use until you hit something like 1000+ requests/month, which
you're very unlikely to hit since Majel implements caching to limit your
requests.

| Parameter     | Type    | Required | Options                                                                                                                            | Default              | Notes                                                                                                                                                                                                                                                        |
|---------------|---------|----------|------------------------------------------------------------------------------------------------------------------------------------|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| key           | string  | Yes      |                                                                                                                                    |                      | Check out the [RapidAPI site](https://rapidapi.com/utelly/api/utelly/pricing) to get a free API key.  Note that a credit card will probably be requested (in case you breech that 1000/mo limit) but for what it's worth, I've never had a charge from them. |
| service_order | array   | No       | `netflix`, `prime`                                                                                                                 | [`netflix`, `prime`] | In the event that the show/movie you're looking for is found on *both* Netflix and Amazon Prime Video, which would you prefer takes precedence?  The default is `netflix`, then `prime`, because Jeff Bezos is a terrible person.                            |
| country       | boolean | Yes      | `ar`, `at`, `be`, `br`, `ca`, `de`, `es`, `fr`, `ie`, `id`, `it`, `is`, `kr`, `my`, `mx`, `no`, `nl`, `pt`, `se`, `sg`, `uk`, `us` |                      | The country you live in.  Utelly's support is regional, so you're limited to specific country codes.  If you're using a VPN for location fakery, you'll want to use the country you're pretending to be in, rather than the one you're actually in.          |
