# Frequently Asked Questions

## Can you make this work in Wayland?

Not at the moment, no.  Majel depends heavily on [pyautogui](pyautogui.readthedocs.io/),
a library that does handy things like control your mouse and keyboard, and [it only supports Xorg](https://github.com/asweigart/pyautogui/issues/111).
There are Wayland-friendly alternatives out there, (see the above pyautogui
issue), but all of them require root and none of them have pyautogui's
`.click("/path/to/image.png")` feature which Majel uses heavily.

So for the moment, you have to use Xorg.  If you have some ideas around how to
support Wayland though, please send a merge request!


## Can you make this work with a DE other than GNOME?

There's nothing really preventing this other than a core understanding of other
desktop environments.  Majel is based on leveraging the idea of "workspaces"
(sometimes called multiple desktops) where each app is opened on a particular
workspace and each handler has a `.move_to_workspace()` method that takes you
there before doing its thing.  Supporting KDE or any other Linux-based DE that
uses workspaces should then be a trivial process of just figuring out how to
get `config.py` to be aware of which key commands are required to switch to the
appropriate workspace.


## What about Windows or Mac?

Maybe?  I have zero interest in supporting proprietary operating systems that
don't respect their users' freedom though, so someone else will have to do it.


## Can you re-license it under ${MY_FAVOURITE_LICENSE}?

Unlikely.  I like the AGPL because it protects my rights and have no interest
in re-licensing it under something like MIT that would allow a private company
to just take this code, repackage it like it was theirs and sell it off.  If a
GPL project wanted to adopt it for something else though, I could be persuaded.
