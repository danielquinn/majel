# Contributing

Majel is Free software licensed under the [AGPL](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0)).
So long as you're cool with the terms of that license, you're welcome to
contribute to this project.


## Options for Contributing

You don't have to know how to code to contribute!


### Bug Reports

Bug reports from non-technical users are especially helpful because they help
developers see how non-developers use the software and (perhaps more
importantly) expect it to work.  Creating a bug report is easy:

1. [Open a free GitLab account](https://gitlab.com/users/sign_up).
2. Go to the [issues page](https://gitlab.com/danielquinn/majel/-/issues?sort=created_date&state=opened)
   for this project.
3. If filing a bug report, check the list to make sure your bug hasn't already
   been reported.
4. If your bug is new, or you're making a feature request, click `New Issue` on
   the top-right.
5. In filling out the form that comes next, try to be as thorough as possible.
   If it's a bug report, explain what you were doing before the bug happened,
   and critically, *how to reproduce the bug*.  If you're making a feature
   request, then try to give some background as to how/why this feature would
   be useful.
6. Submit the form, and check back from time-to-time in case a developer wants
   to ask you questions.

!!! note

    Please be nice when communicating with others.  No one is getting paid here
    and being mean will just result in your issue being deleted.


### Documentation

They say that a Free software project lives and dies by its documentation.
Terrible code can be fixed, but terrible (or non-existent) documents typically
mean that no one is even going to try to use the code in the first place.

If you run into problems getting Majel started, please consider making a merge
request to amend the docs to include how you figured things out.  Sometimes all
that's necessary is a paragraph on the [Troubleshooting](troubleshooting.md)
page, or maybe the docs you were following weren't sufficiently clear and they
need direct changes.  Whatever you can do to make the next person's life
easier, why not do it?


#### Translations

Currently, *everything* in this project is in English, but it doesn't have to be
that way.  There's nothing inherent in the code to limit us to one language,
but adoption in additional languages in unlikely to take if the docs are
English-only.

If you're multi-lingual and would like to write some docs, post an issue and we
can talk about how best to do that.


### More (and better!) Handlers

Majel has a short list of supported handlers at the moment, mostly revolving
around playing video.  On the roadmap, is a plan to support GNOME Music and/or
Rhythmbox, or Kodi for playing music, and some sort of dictation handler to
allow people to type-by-voice which could be leveraged in something like an
email handler.  If you have an idea for something else, why not oppen an issue
in GitLab to sort out how it could be done?

Have a look at the [architecture](architecture.md) and
[extending](extending.md) sections for more information.


### Plugins

Even if your idea doesn't make sense as an addition to Majel directly, you can
still develop it as a plugin.  Have a look at the [architecture](architecture.md)
and [extending](extending.md) sections for more information if this sounds like
something for you.


### Extending Other Functionality

You're not limited to adding handlers of course.  The recorder likely needs
work to support things like dictation, and adding support for different/better
STT and TTS engines would also be nice.  Other ideas are also welcome!  Again,
the [architecture](architecture.md) and [extending](extending.md) sections are
a good place to start.
