from typing import Dict, Optional, Sequence, Tuple, Union

import kodijson

from fuzzywuzzy import fuzz
from fuzzywuzzy.process import extractOne
from requests.exceptions import ConnectionError as RequestsConnectionError

from ..config import config
from ..logger import Loggable


EpisodeList = Sequence[Dict[str, Union[int, str]]]


class Kodi(Loggable):
    """
    A thin wrapper around kodijson that handles tv show lookups in a friendlier
    interface.  For low-level documentation on the Kodi API, you wanna go here:
    https://kodi.wiki/view/JSON-RPC_API/v12
    """

    # Kodi's search API isn't all that smart, and won't, for example, find
    # "Star Trek Deep Space 9" when you search for "Deep Space Nine".  To work
    # around this, we just ask Kodi for everything and use fuzzywuzzy to
    # extract the best match.
    SEARCH_EVERYTHING_FILTER = {
        "field": "title",
        "operator": "startswith",
        "value": "",
    }

    def __init__(self):

        self.api = None
        self.is_enabled = False

        required = (
            config.handlers.kodi.username,
            config.handlers.kodi.password,
            config.handlers.kodi.endpoint,
            config.handlers.kodi.is_enabled,
        )
        if not all(required):
            self.logger.warning(
                "Kodi has not been sufficiently configured.  "
                "Its helper library therefore cannot communicate with Kodi.\n"
                "== Current Configuration ======================\n"
                "  username: %s\n"
                "  password: %s\n"
                "  endpoint: %s\n"
                "  is_enabled: %s\n",
                *required,
            )
            return

        self.is_enabled = True
        self.api = kodijson.Kodi(
            config.handlers.kodi.endpoint,
            username=config.handlers.kodi.username,
            password=config.handlers.kodi.password,
        )

    def test_connection(self) -> bool:
        try:
            return bool(self.api.Files.GetSources(media="video"))
        except RequestsConnectionError:
            self.is_enabled = False
            return False

    def search(self, title: str) -> Tuple[str, int]:
        """
        Assume we're looking for movies first, try that and then move onto tv
        shows if that doesn't return anything useful.
        """

        if movie := self._find_closest_match(title, "movies"):
            return movie

        if show := self._find_closest_match(title, "tvshows"):
            show_id = show[1]
            self.logger.info("Found show id: %s", show_id)
            if episode := self._find_episode(show_id):
                return episode

    def play(self, lookup: str, identifier: int):
        """
        Tell Kodi to play something based on a lookup/id pair.  For example:
          * "movieid", 123
          * "tvshowid", 321
        """
        return self.api.Player.Open(item={lookup: identifier})

    def stop(self):
        """
        The "1" here is a guess.  Kodi appears to accept anything between 0 and
        2 inclusive, but 0 doesn't work and 1 and 2 both stop the video for me.
        Examples I've found online all reference `1` though, so that's what I'm
        going with.
        """
        self.api.Player.Stop(playerid=1)

    def _find_closest_match(
        self,
        title: str,
        key: str,
    ) -> Optional[Tuple[str, int]]:
        """
        Query Kodi's `key` API (either `movies` or `tvshows`), searching for
        `title`.  If we find something suitable, return a dict of either:

          {"movieid": n}

        or

          {"tvshowid": n}

        """

        fn = self.api.VideoLibrary.GetTVShows
        id_name = "tvshowid"
        if key == "movies":
            fn = self.api.VideoLibrary.GetMovies
            id_name = "movieid"

        self.logger.info("Querying Kodi with %s:%s", key, title)
        listing = {
            _["label"].lower(): _[id_name]
            for _ in fn(filter=Kodi.SEARCH_EVERYTHING_FILTER)["result"][key]
        }

        result = extractOne(
            title.lower(),
            listing.keys(),
            scorer=fuzz.token_set_ratio,
        )

        if not result:
            return None

        match, confidence = result

        if confidence > 75:
            self.logger.info(
                "Record found with %s%% confidence: %s",
                confidence,
                match,
            )
            return id_name, listing[match]

        return None

    def _find_episode(self, show_id: int) -> Optional[Tuple[str, int]]:

        episodes = self._get_all_episodes(show_id)

        if not episodes:
            self.logger.info(
                "No unwatched episodes found.  Wiping the watched status."
            )
            self._reset_show_watched_status(show_id)
            episodes = self._get_all_episodes(show_id)
            if not episodes:
                self.logger.info(
                    "It looks like there aren't any episodes for that show in "
                    "your library, which is... weird."
                )
                return None

        for ep in episodes:
            if ep["playcount"] == 0:
                return "episodeid", ep["episodeid"]

        return "episodeid", episodes[0]["episodeid"]

    def _get_all_episodes(
        self,
        show_id: int,
        only_unwatched: bool = True,
    ) -> EpisodeList:

        play_count = 1
        if not only_unwatched:
            play_count = 999

        return self.api.VideoLibrary.GetEpisodes(
            tvshowid=show_id,
            properties=["season", "episode", "playcount"],
            filter={
                "field": "playcount",
                "operator": "lessthan",
                "value": f"{play_count}",
            },
        )["result"].get("episodes", [])

    def _reset_show_watched_status(self, show_id: int) -> None:
        for ep in self._get_all_episodes(show_id, only_unwatched=False):
            episode_id = ep["episodeid"]
            self.logger.debug(
                'Setting s%de%d - "%s" to unwatched',
                ep["season"],
                ep["episode"],
                ep["label"],
            )
            self.api.VideoLibrary.SetEpisodeDetails(
                episodeid=episode_id,
                resume={
                    "position": 0,
                    "total": 0,
                },
                playcount=0,
            )


kodi = Kodi()
