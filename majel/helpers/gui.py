from enum import Enum
from pathlib import Path
from time import sleep
from typing import Optional, Tuple

import pyautogui

from ..logger import Loggable


class Region(Enum):

    TOP_LEFT = (0, 0, 33, 33)
    TOP_CENTRE = (0, 33, 33, 33)
    TOP_RIGHT = (0, 66, 33, 33)

    MIDDLE_LEFT = (33, 0, 33, 33)
    MIDDLE_CENTRE = (33, 33, 33, 33)
    MIDDLE_RIGHT = (33, 66, 33, 33)

    BOTTOM_LEFT = (66, 0, 33, 33)
    BOTTOM_CENTRE = (66, 33, 33, 33)
    BOTTOM_RIGHT = (66, 66, 33, 33)

    TOP = (0, 0, 100, 33)
    MIDDLE = (33, 0, 100, 33)
    BOTTOM = (66, 0, 100, 33)

    LEFT = (0, 0, 33, 100)
    CENTRE = (0, 33, 33, 100)
    RIGHT = (0, 66, 33, 100)

    ALL = (0, 0, 100, 100)


class Button(Enum):
    LEFT = "left"
    RIGHT = "right"
    MIDDLE = "middle"


class wait_for_pixel(Loggable):
    """
    Given some coordinates, we get the colour currently there, then wait until
    that colour has changed.
    """

    MAX_LOOPS = 120  # Roughly 2 minutes

    def __init__(
        self, name: str, coordinates: Optional[Tuple[int, int]] = None
    ):
        self.name = name
        self.coordinates = coordinates
        if not self.coordinates:
            resolution = pyautogui.size()
            self.coordinates = resolution[0] // 2, resolution[1] // 2
        self.pixel = None

    def __enter__(self) -> None:
        self.pixel = pyautogui.pixel(*self.coordinates)
        self.logger.info("Starting pixel: %s", tuple(self.pixel))

    def __exit__(self, *_):

        count = 0
        check = tuple(self.pixel)

        sleep(0.5)

        self.logger.info(
            "Waiting for pixel to change for %s at %s",
            self.name,
            self.coordinates,
        )

        while pyautogui.pixelMatchesColor(*self.coordinates, check):

            self.logger.info(
                "%s is waiting (%s/%s)...",
                self.name,
                count,
                self.MAX_LOOPS,
            )

            sleep(1)

            count += 1
            if count > self.MAX_LOOPS:
                self.logger.warning(
                    "The pixel check for %s never appeared to change, so "
                    "we're just assuming that everything is ok.",
                    self.name,
                )
                return

        self.logger.info(
            "Finished waiting for the %s pixel to change", self.name
        )


class GUI(Loggable):
    """
    A friendlier wrapper around pyautogui that does handy things like keep
    looking for a thing to click on for a while before giving up.
    """

    ATTEMPT_LIMIT = 10

    def __init__(self):
        self.resolution = pyautogui.size()
        self.centre = self.resolution[0] // 2, self.resolution[1] // 2

    def click_image(
        self,
        image: Path,
        button: Button = Button.LEFT,
        region: Region = Region.ALL,
        confidence: float = 0.8,
    ) -> None:

        position = self.wait_for_image(
            image=image,
            region=region,
            confidence=confidence,
        )

        if not position:
            self.logger.error("Failed to click on %s", image)
            return

        pyautogui.click(position, button=button.value)

    def wait_for_image(
        self,
        image: Path,
        region: Region = Region.ALL,
        confidence: float = 0.8,
        attempt: int = 0,
    ) -> Optional[Tuple[int, int]]:
        """
        Wait around for `seconds`, looking for `image` in `region`.
        """

        if attempt >= self.ATTEMPT_LIMIT:
            self.logger.error("Couldn't find %s.  We're giving up.", image)
            return None

        top, left, width, height = region.value
        screen_width, screen_height = pyautogui.size()

        r = (
            screen_width * top // 100,
            screen_height * left // 100,
            screen_width * width // 100,
            screen_height * height // 100,
        )

        self.logger.info("Searching for %s on the screen at %s", image, r)

        image_path = image.absolute().as_posix()
        centre = pyautogui.locateCenterOnScreen(
            image_path,
            region=r,
            confidence=confidence,
        )
        if centre:
            self.logger.info("Found %s at %s", image, centre)
            return centre

        self.logger.warning(
            "Couldn't find %s.  Waiting to see if it turns up", image
        )

        sleep(0.5)

        return self.wait_for_image(
            image=image,
            region=region,
            confidence=confidence,
            attempt=attempt + 1,
        )

    def type(self, string: str, submit=True) -> None:
        self.logger.info("Typing: %s%s", string, "␍" if submit else "")
        to_type = list(string)
        if submit:
            to_type += ["enter"]
        pyautogui.typewrite(to_type)

    @staticmethod
    def click(coordinates: Tuple[int, int], duration: float = 0.0) -> None:
        pyautogui.click(*coordinates, duration=duration)

    @staticmethod
    def move_to(coordinates: Tuple[int, int], duration: float = 0.0) -> None:
        pyautogui.moveTo(*coordinates, duration=duration)

    def move_out_of_the_way(self) -> None:
        self.move_to((self.centre[0], self.resolution[1]))  # Centre bottom


gui = GUI()
