class ProgrammingError(Exception):
    pass


class CantHandleError(Exception):
    pass
