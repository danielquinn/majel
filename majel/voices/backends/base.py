from ...logger import Loggable


class VoiceBackend(Loggable):

    PLAY_CHUNK = 1024

    def __init__(self) -> None:
        self.tts_is_available = False
        self.stt_is_available = False

    def __str__(self) -> str:
        return self.__class__.__name__.replace("VoiceBackend", "")

    def tts(self, text: str) -> None:
        pass

    def stt(self, audio) -> str:
        pass
