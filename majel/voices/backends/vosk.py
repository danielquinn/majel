import json
import zipfile

from pathlib import Path
from tempfile import NamedTemporaryFile
from urllib.request import urlretrieve

from vosk import KaldiRecognizer, Model

from ...config import config
from ...helpers.noisy import silence
from .base import VoiceBackend


class VoskVoiceBackend(VoiceBackend):

    # This should be set to the name of the model you wish to use, which you
    # can find here: https://alphacephei.com/vosk/models -- a good choice for
    # strong machines is "vosk-model-en-us-0.22", while a Raspberry
    # Pi would probably want to use "vosk-model-small-en-us-0.15".
    SOURCE = config.voice.vosk.source
    SOURCE_BASE = "https://alphacephei.com/vosk/models"

    CACHE = Path.home() / ".cache" / "majel" / "voices" / "vosk"

    def __init__(self):

        super().__init__()

        if not config.voice.preferences.stt == "vosk":
            self.logger.debug(
                'The STT backend is set to "%s", so the Vosk backend will not '
                "be loaded.",
                config.voice.preferences.stt,
            )
            return

        self.logger.info(
            'The STT backend is set to "%s", so that backend will be loaded '
            "now.",
            config.voice.preferences.stt,
        )

        if not self.SOURCE:
            self.logger.warning(
                "No source value defined for the Vosk voice.  "
                "This module is disabled."
            )
            return

        self.CACHE.mkdir(parents=True, exist_ok=True)

        model_path = self.CACHE / self.SOURCE
        if not model_path.exists():
            self.logger.warning("No Vosk model found.  Downloading one now.")
            with NamedTemporaryFile() as f:
                urlretrieve(f"{self.SOURCE_BASE}/{self.SOURCE}.zip", f.name)
                with zipfile.ZipFile(f.name, "r") as zip_ref:
                    # This zip file contains a single folder by the same name,
                    # so we extract to the parent, which results in the files
                    # we want living in the `model_path`.
                    self.logger.info("Model data downloaded.  Decompressing.")
                    zip_ref.extractall(model_path.parent)

        with silence():
            self.model = Model(str(model_path))

        self.recogniser = KaldiRecognizer(self.model, 16000)

        self.stt_is_available = True
        self.tts_is_available = False

        self.logger.info("STT with Vosk is ready")

    def stt(self, path: Path) -> str:

        self.logger.info("Doing STT with Vosk")

        with path.open("rb") as wf:

            wf.read(44)  # Skip header

            while True:

                data = wf.read(4000)

                if len(data) == 0:
                    break

                if self.recogniser.AcceptWaveform(data):
                    result = json.loads(self.recogniser.Result())
                    return result["text"]

            return json.loads(self.recogniser.FinalResult())["text"]
