import os

from pathlib import Path
from shutil import which
from subprocess import PIPE, Popen, run
from time import sleep
from typing import Optional, Sequence, Tuple

import pyautogui

from ...helpers.gui import Region, gui
from ..workspace import WorkspaceHandler


class BrowserHandler(WorkspaceHandler):

    PROFILE_NAME: str
    BINARY_NAMES = (Sequence[str],)
    START_URL: str

    BINARY_NOT_FOUND_MESSAGE: str
    FULLSCREEN: bool = True

    binary: Optional[str]
    process: Popen

    def __init__(self, manager):
        self.binary = self._find_binary()
        self.is_playing = False
        super().__init__(manager)

    @property
    def is_enabled(self) -> bool:

        is_enabled = super().is_enabled

        if is_enabled:

            if not self.PROFILE_NAME:
                self.logger.warning(
                    "You must set a profile name or the %s cannot run.", self
                )
                return False

            if not self.binary:
                self.logger.warning(self.BINARY_NOT_FOUND_MESSAGE)
                return False

            return True

        return False

    def handle(self, command: str) -> None:
        raise NotImplementedError()

    def pre_setup(self):
        pass

    def setup(self):

        self.logger.info(
            'Setting up a browser with the profile "%s"', self.PROFILE_NAME
        )

        self.pre_setup()
        self.process = Popen(
            (self.binary, *self.get_cli_arguments()),
            stdout=PIPE,
            stderr=PIPE,
        )

    def on_ready(self) -> None:
        if self.FULLSCREEN:
            self.go_fullscreen()
        self.go_to(self.get_start_url())

    def get_cli_arguments(self) -> Tuple[str, ...]:
        return ()

    def get_start_url(self):
        return self.START_URL

    def go_fullscreen(self):
        self.logger.debug("Going full-screen")
        pyautogui.hotkey("f11")

    def go_to(
        self,
        url: str,
        wait_for: Optional[Path] = None,
        region: Region = None,
    ) -> None:

        self.logger.info("Going to %s", url)

        pyautogui.hotkey("ctrlleft", "l")
        sleep(0.5)
        pyautogui.typewrite(url)
        sleep(0.25)
        pyautogui.hotkey("enter")

        if wait_for:

            kwargs = {}
            if region:
                kwargs["region"] = region

            if not gui.wait_for_image(wait_for, **kwargs):
                self.logger.error("The ready image wasn't found")

    def stop(self):
        if self.is_playing:
            self.logger.info("Stopping")
            self.switch_to_workspace()
            self.go_to(self.get_start_url())
            self.is_playing = False

    def shutdown(self):
        if self.process:
            self.process.kill()

    def _find_binary(self) -> Optional[str]:
        for option in self.BINARY_NAMES:
            self.logger.debug("Looking for %s", option)
            if path := which(option):
                self.logger.debug("Found: %s", path)
                return path


class FirefoxHandler(BrowserHandler):

    BINARY_NAMES = (
        "firefox",
        "firefox-developer-edition",
        "firefox-nightly",
        "firefox-esr",
    )

    BINARY_NOT_FOUND_MESSAGE: str = "No Firefox installation could be found."

    def handle(self, command: str) -> None:
        raise NotImplementedError()

    def pre_setup(self) -> None:
        """
        Firefox is annoying about its profile naming.  A profile named `Majel`
        is typically named something like `kl9o93bo.Majel` so we have to be
        creative in how we detect its existence.
        """

        path = (Path.home() / ".mozilla" / "firefox").glob(
            f"*.{self.PROFILE_NAME}"
        )

        try:
            next(path)
        except StopIteration:
            run(
                (
                    self.binary,
                    "-CreateProfile",
                    self.PROFILE_NAME,
                    "--new-instance",
                )
            )

    def get_cli_arguments(self) -> Tuple[str, ...]:
        return "--new-instance", "-P", self.PROFILE_NAME


class ChromiumHandler(BrowserHandler):

    BINARY_NAMES = (
        "chromium-armhf",
        "chromium",
        "chromium-browser",
        "google-chrome",
        "google-chrome-stable",
    )

    BINARY_NOT_FOUND_MESSAGE: str = "No Chromium installation could be found."

    def handle(self, command: str) -> None:
        raise NotImplementedError()

    def _find_binary(self) -> str:

        selected = super()._find_binary()

        if selected and Path(selected).name == "chromium-armhf":
            # We do some creative stuff with this in .get_cli_arguments()
            return "docker"

        return selected

    def get_cli_arguments(self) -> Tuple[str, ...]:
        """
        On most systems, this a nice straightforward process, but on aarch64
        machines that've installed the chromium-docker workaround, we have to
        do some creative hackery to get our home directory bits mounted inside
        the running container.
        """
        profile_path = (
            Path().home()
            / ".config"
            / "majel"
            / "chromium-profiles"
            / self.PROFILE_NAME
        )

        if not Path(self.binary).name == "docker":
            return (
                "--no-default-browser-check",
                f"--user-data-dir={profile_path}",
            )

        # The following is just replicating the shell script wrapper in Python
        # to allow us to replace the volume

        ips = run(("hostname", "-i"), stdout=PIPE).stdout.decode().split()
        ip = [_ for _ in ips if not _.strip() == "127.0.0.1"][0]

        image = (
            run(
                (
                    "docker",
                    "images",
                    "-q",
                    "hthiemann/chromium-armhf:latest",
                ),
                stdout=PIPE,
            )
            .stdout.decode()
            .split()
        )[0]

        run(("xhost", "+local:docker"))

        # fmt: off
        return (
            "run",
            "--rm",
            "--privileged",
            "-e", f"DISPLAY=unix{os.environ['DISPLAY']}",
            "-e", f"PULSE_SERVER=tcp:{ip}:4713",
            "-e", "PULSE_COOKIE_DATA=''",
            "-v", f"{Path.home() / '.config' / 'chromium'}:/home/mediaguy/.config/chromium",  # NOQA: E501
            "-v", f"{Path.home() / '.cache' / 'chromium'}:/home/mediaguy/.cache/chromium",  # NOQA: E501
            "-v", f"{Path.home() / '.config' / 'majel' / 'chromium-profiles'}:/home/mediaguy/.config/majel/chromium-profiles",  # NOQA: E501
            "-v", "/tmp/.X11-unix:/tmp/.X11-unix",
            "-v", "/dev:/dev", "-v" "/run:/run",
            "-v", "/etc/machine-id:/etc/machine-id",
            "--ipc=host",
            image,
            "/usr/bin/chromium-browser",
            "--no-default-browser-check",
            "--flag-switches-begin",
            "--enable-gpu-rasterization",
            "--enable-features=VaapiVideoDecoder",
            "--flag-switches-end",
            f"--user-data-dir=/home/mediaguy/.config/majel/chromium-profiles/{self.PROFILE_NAME}",  # NOQA: E501
            "--user-agent='Mozilla/5.0 (X11; CrOS armv7l 12607.82.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.123 Safari/537.36'"  # NOQA: E501
        )
        # fmt: on

    def pre_setup(self):
        paths = (
            Path.home() / ".config" / "chromium",
            Path.home() / ".cache" / "chromium",
            Path.home() / ".config" / "majel" / "chromium-profiles",
        )
        for path in paths:
            path.mkdir(exist_ok=True, parents=True)

    def go_to(
        self,
        url: str,
        wait_for: Optional[Path] = None,
        region: Optional[Tuple[int, int, int, int]] = None,
    ) -> None:
        self.go_fullscreen()
        super().go_to(url, wait_for=wait_for)
        self.go_fullscreen()
