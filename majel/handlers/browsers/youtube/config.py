from typing import Optional

from ....config import HandlerConfig


class YoutubeConfig(HandlerConfig):
    profile: str = "YouTube"
    key: Optional[str]
    workspace: int = 4
    is_enabled: bool = False
