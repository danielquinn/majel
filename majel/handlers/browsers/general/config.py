from ....config import HandlerConfig


class BrowserConfig(HandlerConfig):
    profile: str = "Majel"
    workspace: int = 1
    is_enabled: bool = False
