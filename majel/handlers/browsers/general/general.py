import re
import time

import pyautogui

from ....config import config
from ..base import FirefoxHandler


class BrowserHandler(FirefoxHandler):
    """
    BrowserHandlers don't directly manipulate a browser but rather accept
    commands from the user (like any other handler) and translate those into
    orders for the appropriate BrowserManager thread.
    """

    PRIORITY = 0.5

    CONFIG = config.handlers.browser
    REQUIRED_CONFIG = ("is_enabled",)

    WORKSPACE = CONFIG.workspace

    PROFILE_NAME = config.handlers.browser.profile
    START_URL = "https://danielquinn.org/"
    FULLSCREEN = False

    AWESOME_BAR = {
        "google": "@google",
        "bing": "@bing",
        "duck duck go": "@ddg",
        "wikipedia": "@wikipedia",
        "amazon": "@amazon",
        "the web": "@ddg",
        "my bookmarks": "*",
    }
    TOKENISER = re.compile(
        r"^((?P<goto>go to)|search (?P<source>"
        + "|".join(AWESOME_BAR.keys())
        + r") for) (?P<subject>.*)$"
    )

    def __str__(self) -> str:
        return "Firefox"

    def can_handle(self, command: str) -> bool:
        return bool(self.TOKENISER.match(command))

    def handle(self, command: str) -> None:
        self.logger.info('Attempting to handle "%s" with the browser', command)
        self.switch_to_workspace()
        m = self.TOKENISER.match(command)
        if m.group("goto"):
            self._handle_goto(m.group("subject"))
        else:
            self._handle_search(m.group("source"), m.group("subject"))

    def _handle_goto(self, subject: str) -> None:
        pyautogui.hotkey("ctrlleft", "l")
        pyautogui.typewrite(subject)
        pyautogui.hotkey("enter")

    def _handle_search(self, source: str, subject: str) -> None:

        pyautogui.hotkey("ctrlleft", "l")

        if source == "my bookmarks":

            pyautogui.typewrite(f"* {subject}", interval=0.1)
            pyautogui.hotkey("down")
            time.sleep(0.1)

        else:

            pyautogui.typewrite(self.AWESOME_BAR[source], interval=0.1)
            pyautogui.hotkey("tab")
            pyautogui.typewrite(subject, interval=0.1)

        pyautogui.hotkey("enter")
