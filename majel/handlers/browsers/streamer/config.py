from typing import Optional, Sequence

from ....config import APIConfig


class UtellyConfig(APIConfig):
    key: Optional[str]
    country: Optional[str]
    service_order: Sequence[str] = ("netflix", "prime")
    is_enabled: bool = False
