from typing import Optional

from .....config import HandlerConfig


class PrimeConfig(HandlerConfig):
    profile: str = "Prime"
    region: Optional[str]
    workspace: int = 3
    is_enabled: bool = False
