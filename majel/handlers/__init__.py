#
# These imports are a hack.
#
# Much of Majel's logic is based on interrogating a parent class to find all of
# its subclasses, with which we then do things like instantiate those children
# to be able to handle commands of varying types.
#
# As importing these classes here is a hack, none of these should be imported
# from this file if you're doing something with these classes.  If you want to
# import a class that's listed here, you should do so by importing it from the
# actual file it's in.
#
# If I figure out a way to do the loading without pre-importing everything like
# this, these imports will disappear without notice.
#

from .browsers.general.config import BrowserConfig  # NOQA: F401
from .browsers.general.general import BrowserHandler  # NOQA: F401
from .browsers.streamer.config import UtellyConfig  # NOQA: F401
from .browsers.streamer.netflix.config import NetflixConfig  # NOQA: F401
from .browsers.streamer.netflix.netflix import NetflixHandler  # NOQA: F401
from .browsers.streamer.prime.config import PrimeConfig  # NOQA: F401
from .browsers.streamer.prime.prime import PrimeHandler  # NOQA: F401
from .browsers.youtube.config import YoutubeConfig  # NOQA: F401
from .browsers.youtube.youtube import YoutubeHandler  # NOQA: F401
from .kodi.config import KodiConfig  # NOQA: F401
from .kodi.kodi import KodiHandler  # NOQA: F401
from .open.open import OpenHandler  # NOQA: F401
from .play.play import PlayHandler  # NOQA: F401
from .say.say import SayHandler  # NOQA: F401
from .unhandled.unhandled import UnhandledHandler  # NOQA: F401
from .user.user import UserHandler  # NOQA: F401
