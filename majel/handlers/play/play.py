import concurrent.futures

from typing import Optional, Tuple, Union

from ...config import config
from ...exceptions import CantHandleError
from ...helpers.kodi import kodi
from ...helpers.utelly import utelly
from ..base import Handler


Attempt = Optional[Tuple[str, Union[Tuple[str, int], str]]]


class PlayHandler(Handler):

    PRIORITY = 0.2

    RESPONSE_KODI = "kodi"
    RESPONSE_NETFLIX = utelly.SOURCE_NETFLIX
    RESPONSE_PRIME = utelly.SOURCE_PRIME

    def can_handle(self, command: str) -> bool:
        return command.startswith("play ")

    def handle(self, command: str) -> None:

        query = command[5:]
        result = self.search(query)
        if not result:
            raise CantHandleError(
                "Nothing was found for {query} in any of the available "
                "media services."
            )

        service, address = result
        self.logger.info("Found %s result: %s", service, address)

        if service == self.RESPONSE_KODI:
            return self.manager.execute("kodi:{} {}".format(*address))
        if service == self.RESPONSE_NETFLIX:
            return self.manager.execute(f"netflix:{address}")
        if service == self.RESPONSE_PRIME:
            return self.manager.execute(f"prime:{address}")

    def search(self, query: str):
        """
        Try to find the requested media in both the local installation and the
        Utelly API.  We parallelise this with `concurrent.futures` for
        performance reasons.
        """
        with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
            futures = (
                executor.submit(self.try_kodi, query),
                executor.submit(self.try_streamers, query),
            )
            concurrent.futures.wait(futures)
            for future in futures:
                if r := future.result():
                    return r

    def try_kodi(self, query: str) -> Attempt:
        if not kodi.is_enabled:
            return None
        if result := kodi.search(query):
            return self.RESPONSE_KODI, result
        return None

    def try_streamers(self, query: str) -> Attempt:
        if not config.handlers.netflix.is_enabled:
            if not config.handlers.prime.is_enabled:
                self.logger.info(
                    "Neither Netflix or Amazon are available, so we will "
                    "forego the Utelly lookup"
                )
                return None
        return utelly.search(query)
