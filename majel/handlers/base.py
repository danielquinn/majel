from __future__ import annotations

from threading import Thread
from time import sleep
from typing import Generator

from ..logger import Loggable


class Handler(Loggable, Thread):

    # Set the priority to ensure that higher-priority handlers will handle
    # commands in the event that more than one handler claims that it can
    # handle it.
    PRIORITY: int  # 0 = highest, 1 = lowest

    # As all handlers run in an loop, this is just an easy way to prevent your
    # CPU from going mad.  You probably don't need to change this.
    LOOP_WAIT = 0.5  # Seconds

    # If your subclass requires configuration of any kind, defining it here
    # can make for a nice shorthand as typing `config.handlers.myhandler.thing`
    # everywhere can get a bit ugly.  Also, setting this value allows you to
    # make use of `REQUIRED_CONFIG` below.
    CONFIG = None

    # If your subclass requires that one or more configuration values be set,
    # You can place these values in this tuple.  `.is_enabled()` below will
    # check to ensure that these are all non-empty.
    REQUIRED_CONFIG = ()

    def __init__(self, manager):
        super().__init__(daemon=True)
        self.manager = manager
        self.should_stop = False
        self.__disabled_message_shown = False

    def __str__(self) -> str:
        return self.__class__.__name__.replace("Handler", "")

    @classmethod
    def get_subclass_leaves(cls) -> Generator[Handler]:
        for subclass in cls.__subclasses__():
            yield from subclass.get_subclass_leaves()
            if not subclass.__subclasses__():
                yield subclass

    @property
    def is_enabled(self) -> bool:

        for name in self.REQUIRED_CONFIG:
            if not getattr(self.CONFIG, name):
                if not self.__disabled_message_shown:
                    self.__disabled_message_shown = True
                    table = "== Current Configuration ======================\n"
                    for n in self.REQUIRED_CONFIG:
                        table += f"  {n}: {getattr(self.CONFIG, n)}\n"
                    self.logger.warning(
                        "The %s handler has been disabled due to insufficient "
                        "(or deliberate) configuration.  If this isn't "
                        "intended, have a look at your "
                        "`${HOME}/.config/majel/config.yml` file.\n"
                        "  %s",
                        self,
                        table,
                    )
                return False

        return True

    def can_handle(self, command: str) -> bool:
        return False

    def handle(self, command: str) -> None:
        raise NotImplementedError()

    def stop(self) -> None:
        """
        If this handler is doing something noisy, or something that it would
        make sense that the user would want to stop doing when they issue the
        stop order, it should be handled here.
        """

    def shutdown(self) -> None:
        """
        If the handler needs to do something special when it dies, this is
        where it should happen.
        """

    def run(self) -> None:
        while True:
            if self.should_stop:
                self.shutdown()
                break
            sleep(self.LOOP_WAIT)
