import re

from time import sleep

import pyautogui

from ...config import config
from ..base import Handler


class OpenHandler(Handler):

    PRIORITY = 0.2
    COMMAND_REGEX = re.compile(r"^open (?P<app_name>.+)")

    def __str__(self):
        return "Open"

    def can_handle(self, command: str) -> bool:
        if m := self.COMMAND_REGEX.match(command):
            if m.group("app_name") in config.installed_apps:
                return True
        return False

    def handle(self, command: str) -> None:
        pyautogui.press("winleft")
        sleep(0.5)
        pyautogui.typewrite(list(self.get_app(command)) + ["enter"])

    def get_app(self, command: str) -> str:
        return self.COMMAND_REGEX.match(command).group("app_name")
