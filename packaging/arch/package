#!/usr/bin/env python

import hashlib
import sys
import textwrap

from argparse import ArgumentParser
from pathlib import Path
from shutil import which
from subprocess import run

import toml


class CommandError(Exception):
    pass


class Command:

    ROOT = Path(__file__).absolute().parent.parent.parent.parent
    CONFIG = ROOT / "pyproject.toml"

    TEMPLATE = textwrap.dedent(
        """
            # Maintainer: Daniel Quinn <scratch+archlinux-aur at danielquinn dot org>
    
            pkgname="{{ NAME }}"
            pkgver="{{ VERSION }}"
            pkgrel="1"
            pkgdesc="{{ DESCRIPTION }}"
            arch=("any")
            url="{{ URL }}"
            license=("AGPL3")
            makedepends=("python-setuptools")
            depends=("python"
            "python-gobject"
            "python-fuzzywuzzy"
            "python-kodi-json"
            "python-playsound"
            "python-pyalsaaudio"
            "python-pyaudio"
            "python-pyautogui"
            "python-pydantic"
            "python-pyaml"
            "python-opencv"
            "python-pillow"
            "python-vosk-bin"
            "python-webrtcvad"
            "gnome-shell"
            "gobject-introspection"
            "scrot"
            "tk")
            source=("https://files.pythonhosted.org/packages/source/{{ INITIAL }}/{{ NAME }}/{{ NAME }}-{{ VERSION }}.tar.gz")
            sha512sums=("{{ HASH }}")
    
            build() {
                cd "${srcdir}/{{ NAME }}-{{ VERSION }}"
                python setup.py build
            }

            package() {
                cd "${srcdir}/{{ NAME }}-{{ VERSION }}"
                python setup.py install --root="${pkgdir}/" --optimize=1 --skip-build
                install -Dm 644 LICENSE "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"
                install -Dm 644 README.md "${pkgdir}/usr/share/doc/${pkgname}/README"
            }
        """  # NOQA: E501
    ).strip()

    def __init__(self):

        with self.CONFIG.open() as f:
            self.config = toml.load(f)

        self.name = self.config["tool"]["poetry"]["name"]
        self.version = self.config["tool"]["poetry"]["version"]
        self.description = self.config["tool"]["poetry"]["description"]
        self.homepage = self.config["tool"]["poetry"]["homepage"]
        self.source = self.ROOT / "dist" / f"{self.name}-{self.version}.tar.gz"

        parser = ArgumentParser()
        parser.add_argument("target_dir", type=Path)

        self.args = parser.parse_args()

    def __call__(self, *args, **kwargs) -> int:

        self._check_binaries()
        self._check_source_exists()

        self._write_pkgbuild()
        self._write_srcinfo()

        return 0

    @property
    def pkgbuild(self) -> Path:
        return self.args.target_dir / "PKGBUILD"

    @property
    def srcinfo(self) -> Path:
        return self.args.target_dir / ".SRCINFO"

    @staticmethod
    def _check_binaries():
        if not which("makepkg"):
            raise CommandError("The makepkg program is not installed.")

    def _check_source_exists(self):
        if not self.source.exists():
            raise CommandError(
                "The package doesn't exist yet.  "
                "You should run `poetry build && poetry publish` first."
            )

    def _write_pkgbuild(self):
        with self.source.open("rb") as f:
            package_hash = hashlib.sha512(f.read()).hexdigest()

        with self.pkgbuild.open("w") as f:
            f.write(
                self.TEMPLATE.replace("{{ NAME }}", self.name)
                .replace("{{ VERSION }}", self.version)
                .replace("{{ HASH }}", package_hash)
                .replace("{{ INITIAL }}", self.name[0])
                .replace("{{ DESCRIPTION }}", self.description)
                .replace("{{ URL }}", self.homepage)
            )

    def _write_srcinfo(self):
        with self.srcinfo.open("w") as f:
            run("makepkg --printsrcinfo", shell=True, stdout=f)


if __name__ == "__main__":
    try:
        sys.exit(Command()())
    except CommandError as e:
        sys.stderr.write(str(e) + "\n")
        sys.exit(1)
