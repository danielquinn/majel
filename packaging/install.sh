#!/usr/bin/env bash

set -e

cd "${HOME}" || exit


# Install system dependencies.  Note that we're skipping GNOME, since it's
# assumed that you're already running GNOME-based Manjaro.
yay -S \
  python-poetry \
  gobject-introspection \
  scrot \
  kodi \
  firefox \
  mimic

# Get Majel
mkdir -p ".var/app"
cd ".var/app"
git clone https://gitlab.com/danielquinn/majel-orchestrator.git org.danielquinn.majel


# Use Poetry to install Python dependencies
cd "org.danielquinn.majel"
poetry config virtualenvs.in-project true
poetry install


# The Unix socket
mkdir -m 755 -p .var/run


# Keybindings
gsettings set org.gnome.settings-daemon.plugins.media-keys custom-keybindings "['/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/', '/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/']"

# Keybinding: Listen
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ binding "<Super>c"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ command "gnome-calculator"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom0/ name "Majel Listen"

# Keybinding: Stop
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ binding "<Super>z"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ command "gnome-system-monitor"
gsettings set org.gnome.settings-daemon.plugins.media-keys.custom-keybinding:/org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/custom1/ name "Majel Stop"
