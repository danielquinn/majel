#!/usr/bin/env python

# I wrote this because the recommended script supplied by the Flatpak community
# failed in a multitude of ways.  The code was *really* hard to follow, and
# honestly isn't likely to be easily fixed, so I just rewrote it from scratch.
#
# The good news, is that this script now reliably generates a list of python
# dependencies for a Poetry-based project.  The bad news however is that
# Majel's requirements (access to GNOME's keybindings for example) are such
# that I can't figure out how to get it to work with Flatpak.  So, this code
# will likely sit here 'til I have another project that *can* be flatpacked.

import json
import os
import re
import sys

from logging import getLogger
from pathlib import Path
from subprocess import run
from typing import Dict
from urllib import request

import toml


logger = getLogger(__name__)


class PyPIFailureError(Exception):
    pass


class UnhandledPackageTypeError(Exception):
    pass


class Package:
    """
    Given a package name and version, figure out the source definition that
    we'll use in our dependencies file.  This class is smart enough to
    understand the wheel naming conventions, but makes some assumptions about
    the architecture you want supported.  See `PYTHON_TAG_REGEX` for example.
    """

    PYTHON_TAG_REGEX = re.compile(r".*\b(py3|py38|cp3|cp38)\b.*")
    WHEEL_REGEX = re.compile(
        r"^(?P<distribution>[^-]+)"
        r"-(?P<version>[^-]+)"
        r"(-(?P<build_tag>[^-]+))?"
        r"-(?P<python_tag>[^-]+)"
        r"-(?P<abi_tag>[^-]+)"
        r"-(?P<platform_tag>[^-]+)"
        r".whl$"
    )

    ENDPOINT_TEMPLATE = "https://pypi.org/pypi/{}/json"
    CACHE = Path.home() / ".cache" / "pyflatpak"

    name: str
    version: str
    release: dict

    def __init__(self, definition: Dict[str, str]):

        self.name = definition["name"]
        self.version = definition["version"]

        self.release = self._select_release()

    def __str__(self) -> str:
        return f"{self.name}=={self.version}"

    @property
    def type(self) -> str:

        if self.release["packagetype"] in ("bdist_wheel", "sdist"):
            return "file"

        raise UnhandledPackageTypeError(
            f"Unhandled package type: {self.release['packagetype']}"
        )

    @property
    def url(self) -> str:
        return self.release["url"]

    @property
    def sha256(self) -> str:
        return self.release["digests"]["sha256"]

    @property
    def source_definition(self):
        return {
            "type": self.type,
            "url": self.url,
            "sha256": self.sha256,
        }

    def _select_release(self) -> dict:
        """
        Attempt to find a release in a format we want, preferring wheels over
        sdist over anything else.  If a release isn't available in either of
        these formats, we error-out.
        """

        pypi = json.loads(self._fetch())

        selected = None
        for release in pypi["releases"][self.version]:

            ptype = release["packagetype"]

            if release["yanked"]:
                continue

            if ptype not in ("bdist_wheel", "sdist"):
                continue

            if ptype == "bdist_wheel":

                file_name = re.sub("^.*/", "", release["url"])

                m = self.WHEEL_REGEX.match(file_name)

                if not m:
                    continue

                python_tag = m.group("python_tag")
                if not self.PYTHON_TAG_REGEX.match(python_tag):
                    continue

                platform_tag = m.group("platform_tag")
                if platform_tag == "any":
                    selected = release
                    continue

                if "manylinux" in platform_tag:
                    if "x86_64" in platform_tag:
                        selected = release
                        continue
                    if "i686" in platform_tag:
                        selected = release
                        continue

            if ptype == "sdist":
                if not selected:
                    selected = release

        if not selected:
            raise PyPIFailureError(
                f"No suitable package types could be found for {self}"
            )

        return selected

    def _fetch(self) -> str:

        cache = self.CACHE / f"{self.name}=={self.version}"

        if cache.exists():
            with cache.open() as f:
                return f.read()

        self.CACHE.mkdir(exist_ok=True)

        print(f"Fetching info for {self}")
        response = request.urlopen(self.ENDPOINT_TEMPLATE.format(self.name))
        if not response.status == 200:
            raise PyPIFailureError(f"PyPI had no response for package {self}")

        r = response.read()
        with cache.open("w") as f:
            f.write(r.decode())

        return r


class Command:
    """
    This is using a template where in the end, it probably would have been
    cleaner just to use this script to generate the dependencies and exit.
    """

    PROJECT_ROOT = Path(__file__).parent.parent.parent
    PYPROJECT = PROJECT_ROOT / "pyproject.toml"

    PACKAGING = PROJECT_ROOT / "packaging" / "flatpak"
    TEMPLATE = PACKAGING / "manifest-template.yaml"
    MANIFEST = PACKAGING / "org.danielquinn.Majel.yaml"
    REQUIREMENTS = PACKAGING / "requirements.json"
    DEPENDENCIES = PACKAGING / "python-dependencies.json"

    PIP_INSTALL = (
        "pip3 install "
        "--no-index "
        "--no-deps "
        '--find-links="file://${PWD}" '
        "--prefix=${FLATPAK_DEST}"
    )

    def __init__(self):

        with self.PYPROJECT.open() as f:
            project = toml.load(f)

        self.name = project["tool"]["poetry"]["name"]
        self.version = project["tool"]["poetry"]["version"]
        self.wheel = None  # Set in self.build_python()

        self.requirements = []
        self._ensure_requirements()
        with self.REQUIREMENTS.open() as f:
            for p in json.load(f):
                if p["name"] == self.name:
                    continue
                self.requirements.append(Package(p))

    def _ensure_requirements(self):
        if not self.REQUIREMENTS.exists():
            run(("poetry", "install", "--no-dev"))
            with self.REQUIREMENTS.open("w") as f:
                cmd = ("poetry", "run", "pip", "list", "--format=json")
                run(cmd, stdout=f)
            run(("poetry", "install"))

    def main(self):
        self.build_python()
        self.generate_dependencies()
        self.generate_manifest()
        self.build_flatpak()

    def build_python(self):
        os.chdir(self.PROJECT_ROOT)
        run(("poetry", "build"))
        self.wheel = str(
            next(
                (self.PROJECT_ROOT / "dist").glob(
                    f"{self.name}-{self.version}*.whl"
                )
            ).absolute()
        )

    def generate_dependencies(self):
        with self.DEPENDENCIES.open("w") as f:
            f.write(
                json.dumps(
                    {
                        "name": "python-dependencies",
                        "buildsystem": "simple",
                        "build-commands": [
                            "{} {}".format(
                                self.PIP_INSTALL,
                                " ".join([p.name for p in self.requirements]),
                            )
                        ],
                        "sources": [
                            p.source_definition for p in self.requirements
                        ],
                    }
                )
            )

    def generate_manifest(self):
        with self.TEMPLATE.open() as template:
            with self.MANIFEST.open("w") as manifest:
                manifest.write(
                    template.read().replace("{{ PATH }}", self.wheel)
                )

    def build_flatpak(self):
        os.chdir(self.PACKAGING)
        run(
            (
                "flatpak-builder",
                "--user",
                "--install",
                "--force-clean",
                "build-dir",
                self.MANIFEST,
            )
        )


if __name__ == "__main__":
    try:
        Command().main()
    except Exception as e:
        print(e)
        sys.exit(1)
