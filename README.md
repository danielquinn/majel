# Majel

Automate your desktop with your voice!

[![Documentation](https://img.shields.io/badge/documentation-%F0%9F%93%96-%23009900)](https://danielquinn.gitlab.io/majel)
[![GitLab](https://img.shields.io/badge/source-gitlab-%23FCA121?url=https://gitlab.com/danielquinn/majel)](https://gitlab.com/danielquinn/majel)
[![License](https://img.shields.io/pypi/l/majel)](https://gitlab.com/danielquinn/majel/blob/master/LICENSE)
[![PyPI](https://img.shields.io/pypi/pyversions/majel?logo=python&logoColor=%23ffffff&label=)](https://pypi.org/project/majel/)
[![Flake8](https://img.shields.io/badge/linter-flake8-informational.svg)](https://flake8.pycqa.org/en/latest/)
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Downloads](https://img.shields.io/pypi/dm/majel)](https://pypi.org/project/majel/)
[![Status](https://img.shields.io/pypi/status/majel)](https://pypi.org/project/majel/)
[![Liberapay](https://img.shields.io/liberapay/patrons/danielquinn.svg?logo=liberapay)](https://liberapay.com/danielquinn/)

This project is a labour of love born out of frustration with the alternatives
out there.  If you're looking for something extendable and generic that you can
use to drive pretty much anything on your desktop, this could be it.

At the moment, Majel is mostly a media automator: a way to tell your computer
to play your favourite tv show or find something in your browser bookmarks.
Over time though, I want to leverage its generic API to automate things like
sending emails, scheduling events, making calls, sending texts, or other
easily-made-hands-free things.

The current state of the project is **alpha**, so suggestions and bug reports
are very much appreciated.  Please just create an issue in GitLab and we can
have the conversation there.  Obviously, if you're looking to get features
added, merge requests are greatly appreciated.
