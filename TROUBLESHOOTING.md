# Chromium keeps asking me to restore tabs

1. Go to the `Closed Tab Cache` option in Chromium flags: `chrome://flags/#closed-tab-cache`
2. Mark it as Disabled
3. Close Chromium


# I can't install from PyPI on a Raspberry Pi

When you try to install a Python project across architectures, things get...
tricky.  Many projects, it would seem will often not release packages for
`aarch64` or `armv7l` on PyPI, instead pushing them to GitHub or piwheels.org.
This means that if you're going to install this on a non-x86 machine, there's
likely to be a couple bumps you might have to smooth out.


## `armv7l`

The default Raspberry Pi OS runs the 32-bit `armv7l` architecture (even though
modern Pis are 64-bit capable).  The only package that's proven itself to be a
problem as been `opencv-python`, which doesn't have `armv7l` wheels on [PyPI](https://pypi.org/project/opencv-python/#files),
but *does* have some on [piwheels.org](https://www.piwheels.org/project/opencv-python/).

Thankfully, because of how Raspberry Pis are typically set up to include
piwheels.org as an extra source for wheels, installation should be as easy as:

```shell
$ pip install majel
```

If however you're wanting to *develop* on your Pi, this project's use of Poetry
bumps up against [an annoying bug](https://stackoverflow.com/questions/70564200/how-to-use-python-poetry-across-architectures)
in that project, so development will unfortunately have to look something like
this:

```shell
$ git clone <your fork of this project> majel
$ cd majel
$ poetry build
$ pip install dist/majel-<version>-py3-none-any.whl
[Make your changes]
$ poetry build
$ pip install dist/majel-<version>-py3-none-any.whl
```

...repeat until you have what you need.  I know, it sucks, so if you've got a
better idea, please share it.


## `aarch64`

If you're using a Raspberry Pi with a 64-bit OS like Manjaro, then
`opencv-python` *is* available on PyPI but `vosk-api` [isn't](https://pypi.org/project/vosk/#files).
For that package, you need to source the `aarm64` wheel from their git
repo.  The reasoning for this hasn't been made clear to me yet, but I'm having
a conversation with them about this [here](https://github.com/alphacep/vosk-api/issues/479#issuecomment-1002715227).
Until this is fixed though, installing Majel on an aarch64-based system should
either be done with Arch/Manjaro's `yay -S majel` (which accounts for this
oddity in the installer) or in three steps the old-fashioned way:

```shell
# Install stuff the Python dependencies need on the system.
$ yay -S gobject-introspection gnome-shell scrot tk

# Install Vosk directly from GitHub
$ pip install https://github.com/alphacep/vosk-api/releases/download/v0.3.32/vosk-0.3.32-py3-none-linux_aarch64.whl

# Install majel
$ pip install majel
```

Similarly, if you're developing on Majel using Poetry, you'll need a few extra
steps as well:

```shell
# Install all of the dependencies but fail on vosk
$ poetry install

# Hop into your new virtualenv
$ poetry shell

# Install vosk from GitHub
$ pip install https://github.com/alphacep/vosk-api/releases/download/v0.3.32/vosk-0.3.32-py3-none-linux_aarch64.whl

# Install majel now that all dependencies are in place
$ poetry install
```

> Note that the version numbers used in the above URLs may change over time.
You'll need to note what version Majel is trying to install and amend the URL
to suit before installing.


# Can you make this work in Wayland?

Not at the moment, no.  Majel depends heavily on [pyautogui](pyautogui.readthedocs.io/),
a library that does handy things like control your mouse and keyboard, and [it only supports Xorg](https://github.com/asweigart/pyautogui/issues/111).
There are Wayland-friendly alternatives out there, (see the above pyautogui
issue), but all of them require root and none of them have pyautogui's
`.click("/path/to/image.png")` feature which Majel uses heavily.

So for the moment, you have to use Xorg.  If you have some ideas around how to
support Wayland though, please send a merge request!


# Can you make this work with a DE other than GNOME?

There's nothing really preventing this other than a core understanding of other
desktop environments.  Majel is based on leveraging the idea of "workspaces"
(sometimes called multiple desktops) where each app is opened on a particular
workspace and each handler has a `.move_to_workspace()` method that takes you
there before doing its thing.  Supporting KDE or any other Linux-based DE that
uses workspaces should then be a trivial process of just figuring out how to
get `config.py` to be aware of which key commands are required to switch to the
appropriate workspace.


## What about Windows or Mac?

Maybe?  I have zero interest in supporting proprietary operating systems that
don't respect their users' freedom though, so someone else will have to do it.


# Can you re-license it under ${MY_FAVOURITE_LICENSE}?

Unlikely.  I like the AGPL because it protects my rights and have no interest
in re-licensing it under something like MIT that would allow a private company
to just take this code, repackage it like it was theirs and sell it off.  If a
GPL project wanted to adopt it for something else though, I could be persuaded.
